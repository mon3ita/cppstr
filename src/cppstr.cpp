#include "cppstr.h"

/**
* Converts a given string object to c string.
*
* @param string
* @return char *
*/
char * CppStr::to_cstring(const string& str) {
    unsigned int size = sizeof(char) * str.size() + 1;
    char * result = (char*)malloc(size);
    str.copy(result, str.size());
    result[size] = '\0';
    return result;
}

/**
* Returns the first index at which a searched char is contained. If such char
* doesn't exist in the string, returns -1. The searched interval can be limited from
* left and/or right.
*
* @param string
*   String object in which a char will be searched.
* @param char
*   Char which will be searched.
* @param unsigned int
*   Left end of the interval, by default is set to 0.
* @param unsigned int
*   Right end of the interval, by default is set to 0.
* @return int
*   Index at which the char is contained in the given string, or -1, if it's not found.
*/
int CppStr::ch_idx(const string& str, char ch, unsigned int from, unsigned int to) {
    to = from == to ? str.size() : to;

    for(unsigned int i = from; i < to; ++i)
        if(ch == str[i]) return i;
    return -1;
}

/**
* Compares two strings.
*
* @param string
*   First string.
* @param string
*   Second string.
* @return bool
*   True if, the first string is > than the second,
*   false otherwise.
*/
bool CppStr::operator>(const string& lhs, const string& rhs) {
    unsigned int size = lhs.size() < rhs.size() ? lhs.size() : rhs.size();
    for(unsigned int i = 0; i < size; i++)
        if(lhs[i] < rhs[i])
            return false;
        else if(lhs[i] > rhs[i])
            return true;


    return lhs.size() > rhs.size();
}

/**
* Compares two strings.
*
* @param string
*   First string.
* @param string
*   Second string.
* @return bool
*   True if, the second string is > than the first,
*   false otherwise.
*/
bool CppStr::operator<(const string& lhs, const string& rhs) {
    return !CppStr::operator<(lhs, rhs);
}

/**
* Compares two strings.
*
* @param string
*   First string.
* @param string
*   Second string.
* @return bool
*   True if, the first string is >= than the second,
*   false otherwise.
*/
bool CppStr::operator<=(const string& lhs, const string& rhs) {
    return CppStr::operator<(lhs, rhs) || lhs == rhs;
}

/**
* Compares two strings.
*
* @param string
*   First string.
* @param string
*   Second string.
* @return bool
*   True if, the first string is <= than the second,
*   false otherwise.
*/
bool CppStr::operator<=(const string& lhs, const string& rhs) {
    return CppStr::operator>(lhs, rhs) || lhs == rhs;
}

/**
* Repeats a string.
*
* @param string
*   String object which to be repeated.
* @param unsigned int
*   Number of time which a string to be repeated.
* @return string
*   A new string, containing the result.
*/
string CppStr::operator*(const string& str, unsigned int times) {
    string result = str;
    for(unsigned int i = 1; i < times; i++)
        result.append(str);
    return result;
}

/*
*   Alias for CppStr::operator*
*/
string CppStr::repeat(const string& str, unsigned int times){
    return CppStr::operator*(str, times);
}

/**
* Checks if a given string is a number.
*
* @param string
*   String object, which to be checked.
* @return bool
*   True, if the string contains only digits.
*   False, otherwise
*/
bool CppStr::is_number(const string& str) {
    for(unsigned int i = 0; i < str.size(); i++) {
        int ascii = int(str[i]);
        if(!((ascii >= 97 && ascii <= 122) || (ascii >= 65 && ascii <= 90) ))
            return false;
    }

    return true;
}

string CppStr::lstrip(const string& str, const string& remove) {
    string result;
    string remove_by = const_cast<string&>(remove);

    int len = remove_by.size() % 2 ? (remove_by.size() / 2) + 1 : remove_by.size() / 2;
    if(remove_by.substr(0, len) == remove_by.substr(remove_by.size() / 2, remove_by.size()))
        remove_by = remove_by.substr(0, len);

    int i = 0, j = remove_by.size() - 1;
    while(i < j && remove_by[i++] == remove_by[j--]) ;
    if(i - 1 == j) remove_by = string(1, remove_by[0]);

    if(remove_by.size() > str.size())
        return result; 

    i = 0;
    while(str.substr(i, remove_by.size()) == remove_by) i += remove_by.size();
    result.append(str.substr(i, str.size()));
    return result;
}


string CppStr::rstrip(const string& str, const string& remove) {
    string result;
    string remove_by = const_cast<string&>(remove);

    int len = remove_by.size() % 2 ? (remove_by.size() / 2) + 1 : remove_by.size() / 2;
    if(remove_by.substr(0, len) == remove_by.substr(remove_by.size() / 2, remove_by.size()))
        remove_by = remove_by.substr(0, len);

    int i = 0, j = remove_by.size() - 1;
    while(i < j && remove_by[i++] == remove_by[j--]) ;
    if(i - 1 == j) remove_by = string(1, remove_by[0]);

    if(remove_by.size() > str.size())
        return result; 

    i = str.size();
    while(i > 0 && (str.substr(i - remove_by.size(), remove_by.size()) == remove_by)) i -= remove_by.size();

    result.append(str.substr(0, i));
    return result;
}

string CppStr::strip(const string& str, const string& remove) {
   string result;

    result = CppStr::lstrip(str, remove);
    result = CppStr::rstrip(result, remove);
    return result;
}

string CppStr::tolower(const string& str, unsigned int from, unsigned to) {
    string result;
    to = from == to ? str.size() : to;

    for(unsigned int i = from; i < to; i++) {
        result.push_back(std::tolower(static_cast<unsigned char>(str[i])));
    }
    return result;
}


string CppStr::toupper(const string& str, unsigned int from, unsigned to) {
    string result;
    to = from == to ? str.size() : to;

    for(unsigned int i = from; i < to; i++) {
        result.push_back(std::toupper(static_cast<unsigned char>(str[i])));
    }
    return result;
}

vector<string> split(const string& str, const string& split_by) {
    vector<std::string> result;
    
    for(int i = 0; i < str.size(); ) {
        std::size_t index = str.find(split_by, i);

        if(index != string::npos) {
            result.push_back(str.substr(i, index - i));
            i = index + 1;
        } else {
            result.push_back(str.substr(i));
            break;
        }
    }

    return result;
}

vector<char> to_chars(const string& str) {
    vector<char> chars;
    for(auto ch : str)
        chars.push_back(ch);
    return chars;
}

string CppStr::title(const string& str) {
    string result = CppStr::tolower(title);

    return result;
}