#ifndef CPPSTR_H
#define CPPSTR_H

#include <string>
#include <vector>

using std::string;
using std::vector;

namespace CppStr {
    char * to_cstring(const string&);
    int ch_idx(const string&, const char, unsigned int = 0, unsigned int = 0);

    string operator-(const string&, const string&);
    string operator*(const string&, unsigned int);

    bool operator>(const string&, const string&);
    bool operator<(const string&, const string&);
    bool operator>=(const string&, const string&);
    bool operator<=(const string&, const string&);

    string operator*(const string&, unsigned int);
    string repeat(const string&, unsigned int);
    string shift_left(const string&, unsigned int);
    string shift_right(const string&, unsigned int);
    string reverse(const string&);

    vector<string> split(const string&, const string&);
    vector<string> split(const string&, const char = ' ');
    vector<char> to_chars(const string&);

    string strip(const string&, const string& = "");
    string lstrip(const string&, const string& = "");
    string rstrip(const string&, const string& = "");

    string tolower(const string&, unsigned int = 0, unsigned int = 0);
    string toupper(const string&, unsigned int = 0, unsigned int = 0);
    string title(const string&);
    string normalize(const string&, unsigned int = 0, unsigned int = 0);

    bool is_number(const string&);
    bool contains(const string&, const string&);
    bool contains(const string&, const char);

    string remove(const string&, const string&);
    string remove(const string&, const char);

    string remove_if(const string&); // function
}

#endif