#include "TestCppStr.h"

#include <iostream>

void TestCppStr::testToCString() {
    string test = "Test";
    char * result = CppStr::to_cstring(test);
    result[1] = 'a';
    test[1] = 'a';
    CPPUNIT_ASSERT(result == test);
    free(result);
}

void TestCppStr::testChIndx() {
    string test = "Test";
    int result = CppStr::ch_idx(test, 'e');
    CPPUNIT_ASSERT(result == 1);
}

void TestCppStr::testChIndxNotExist() {
    string test = "Test";
    int result = CppStr::ch_idx(test, 'n');
    CPPUNIT_ASSERT(result == -1);
}

void TestCppStr::testStrGr() {
    string test = "abc";
    string test2 = "abcd";
    CPPUNIT_ASSERT(CppStr::operator>(test2, test));
}