#ifndef TEST_CPP_STR_H
#define TEST_CPP_STR_H

#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/ui/text/TextTestRunner.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/TestRunner.h>
#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/CompilerOutputter.h>
#include <cppunit/XmlOutputter.h>
#include <netinet/in.h>

#include "../src/cppstr.h"

using namespace CppUnit;

class TestCppStr :  public CppUnit::TestFixture {
    CPPUNIT_TEST_SUITE(TestCppStr);
    CPPUNIT_TEST(testToCString);
    CPPUNIT_TEST(testChIndx);
    CPPUNIT_TEST(testChIndxNotExist);
    CPPUNIT_TEST(testStrGr);
    CPPUNIT_TEST_SUITE_END();
public:
    void setUp(void) { }
    void tearDown(void) { }
protected:
    void testToCString();
    void testChIndx();
    void testChIndxNotExist();
    void testStrGr();
};

#endif